/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QtXml>

#include <chrono>
#include <QDebug>

#include "csyntaxhighlighter.h"
#include "ccolorscheme.h"


CSyntaxHighlighter::CSyntaxHighlighter(QTextDocument *doc, QString fileName, CColorScheme *colorScheme)
	: QSyntaxHighlighter(doc)
{
	updateColorScheme(colorScheme,fileName);
}

void CSyntaxHighlighter::updateColorScheme(CColorScheme *colorScheme, QString syntaxFile){
	scheme = colorScheme;
	syntax.append(*loadSyntaxFile(syntaxFile));

	rehighlight();
}

QVector<CSyntaxRule> *CSyntaxHighlighter::loadSyntaxFile(QString fileName)
{
	/*
	 * An syntax xml file format
	 *
	 * CorePadSyntax
	 * |--> data
	 * |    |--> target = value
	 * |--> pattern
	 *      |--> group
	 *           |--> format = color, italic, bold
	 *           |--> item   = begin, end
	 *                |--> pattern
	 */

	QVector<CSyntaxRule> *list = new QVector<CSyntaxRule>();
	QDomDocument doc;
	QFile file(fileName);

	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		qDebug() << "Cannot read file " << fileName;
		return list;
	} else {
		doc.setContent(&file);
		file.close();
	}

	QDomElement root = doc.firstChildElement(); // CorePadSyntax

	QDomElement data = root.firstChildElement(); // data
	// No need to fetch meta data not needed for now

	QDomElement pattern = root.lastChildElement(); // pattern
	QDomNodeList groups = pattern.elementsByTagName("group");

	for (int i = 0; i < groups.count(); i++) {
		QDomNode node = groups.at(i);

		if (node.isElement()) {
			QDomElement element = node.toElement();
			QDomElement temp;
			temp = element.namedItem("format").toElement();

			QTextCharFormat format;
			QString color = temp.attribute("color");
			QString colorCode = scheme->getColor(color);
			format.setForeground(QBrush(QColor(colorCode)));

            if (temp.attribute("italic").length() > 0) {
				if (temp.attribute("italic") == "true") {
					format.setFontItalic(true);
				} else {
					format.setFontItalic(false);
				}
			}

			if (temp.attribute("bold") == "true") {
				format.setFontWeight(QFont::Bold);
			}

			temp = element.namedItem("item").toElement();

			CSyntaxRule rule;
			QString begin = temp.attribute("begin");
			QString end = temp.attribute("end");
			QString value = temp.firstChildElement().text();

            if (!begin.length() && !end.length() && !value.length())
				continue;

			rule.begin = QRegularExpression(begin);
			rule.end = QRegularExpression(end);
			rule.pattern = QRegularExpression(value);

			rule.format = format;

			list->append(rule);
		}
	}

	return list;
}

void CSyntaxHighlighter::highlightBlock(const QString &text)
{
	QRegularExpression rxb;
	QRegularExpression rxe;
	QTextCharFormat cFormat;

	for (CSyntaxRule rule : syntax) {
        if (!rule.pattern.pattern().length()) {
			rxb.setPattern(rule.begin.pattern());
			rxe.setPattern(rule.end.pattern());
			cFormat = rule.format;
			continue;
		}

		QRegularExpressionMatch m = rule.pattern.match(text);

		int index = m.capturedStart();

		while (index >= 0) {
			int length = m.capturedLength();
			setFormat(index, length, rule.format);
			m = rule.pattern.match(text, index + length);
			index = m.capturedStart();
		}
	}

	setCurrentBlockState(0);

	int start = 0;

    if (!rxb.pattern().length() || !rxe.pattern().length()) {
		return;
	}

	if (! rxb.isValid() || ! rxe.isValid()) {
		return;
	}

	int add = 0;

	QRegularExpressionMatch mb = rxb.match(text);

	if (previousBlockState() == 1 || previousBlockState() == 2) {
		start = 0;
		add = 0;
	} else {
		start = mb.capturedStart();
		add = mb.capturedLength();
	}

	while (start >= 0) {
		QRegularExpressionMatch me = rxe.match(text, start + add);

		int end = me.capturedStart();

		int len;

		if (end == -1) {
			setCurrentBlockState(1);
			len = text.length() - start;
		} else if (end >= start) {
			len = end - start + mb.capturedLength() + me.capturedLength();
			setCurrentBlockState(0);
		} else {
			setCurrentBlockState(1);
			len = end - start + me.capturedLength();
		}

		setFormat(start, len, cFormat);

		mb = rxb.match(text, start + len);
		start = mb.capturedStart();
	}
}
