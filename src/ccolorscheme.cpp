/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QDebug>
#include <QSettings>

#include "ccolorscheme.h"

CColorScheme::CColorScheme(QString schemeFile)
{
	loadSchemeFile(schemeFile);
}

QString CColorScheme::getAttributeColor() const
{
	return getColor("attribute");
}

QString CColorScheme::getBackgroundColor() const
{
	return getColor("background");
}

QString CColorScheme::getCaretColor() const
{
	return getColor("caret");
}

QString CColorScheme::getClassColor() const
{
	return getColor("class");
}

QString CColorScheme::getCommentColor() const
{
	return getColor("comment");
}

QString CColorScheme::getCommentDocColor() const
{
	return getColor("comment-doc");
}

QString CColorScheme::getCurrentLineColor() const
{
	return getColor("current-line");
}

QString CColorScheme::getDataTypeColor() const
{
	return getColor("data-type");
}

QString CColorScheme::getErrorColor() const
{
	return getColor("error");
}

QString CColorScheme::getForegroundColor() const
{
	return getColor("foreground");
}

QString CColorScheme::getFunctionColor() const
{
	return getColor("function");
}

QString CColorScheme::getKeyword1Color() const
{
	return getColor("keyword1");
}

QString CColorScheme::getKeyword2Color() const
{
	return getColor("keyword2");
}

QString CColorScheme::getNumberColor() const
{
	return getColor("number");
}

QString CColorScheme::getPreprocessorColor() const
{
	return getColor("preprocessor");
}

QString CColorScheme::getSelectionColor() const
{
	return getColor("selection");
}

QString CColorScheme::getTagColor() const
{
	return getColor("tag");
}

QString CColorScheme::getTextColor() const
{
	return getColor("text");
}

QString CColorScheme::getValueColor() const
{
	return getColor("value");
}

QString CColorScheme::getColor(const QString key) const
{
	return values.value(key);
}

void CColorScheme::loadSchemeFile(QString schemeFile)
{
    if (!schemeFile.length()) {
		schemeFile = QString(":/syntax/dark.colorscheme");
	}

	QSettings *scheme = new QSettings(schemeFile, QSettings::IniFormat);

	QString theme = schemeFile.split('/').last().remove(".colorscheme");
	scheme->beginGroup(theme);

	foreach (QString key, scheme->allKeys()) {
		values.insert(key, scheme->value(key).toString());
	}

	scheme->endGroup();
}
