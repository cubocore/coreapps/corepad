/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>
#include <QTextDocument>
#include <QMimeDatabase>
#include <QLabel>
#include <QFileSystemWatcher>
#include <QThread>

#include "settings.h"

class CColorScheme;
class CEditor;
class QListWidgetItem;

namespace Ui {
	class corepad;
}


class searchText : public QObject {
	Q_OBJECT
public:
	void setSearchCriteria(const QString &keyword, const QString &contents, bool isRegEx);
	void searchNow();
	void stopNow(bool closeNow);

private:
	QString keyword;
	QString contents;
	bool isRegEx;
	bool breakNow = false;

signals:
	void updateCountText(const QString&);
};

class corepad : public QWidget {
	Q_OBJECT

public:
	explicit corepad(QWidget *parent = nullptr);
	~corepad();

	void sendFiles(const QStringList &paths);

private slots:
	void showSideView();
	void on_newTextW_clicked();
	void on_cNew_clicked();
	bool on_cSave_clicked();
	void on_cSaveAs_clicked();
	void on_cCopy_clicked();
	void on_cPaste_clicked();
	void on_cCut_clicked();
	void on_cUndo_clicked();
	void on_cRedo_clicked();
	void on_addDate_clicked();
	void on_search_clicked();
	void on_pinIt_clicked();
	void on_shareIt_clicked();

	void on_nextW_clicked();
	void on_previousW_clicked();
	void on_replaceT_clicked();
	void on_replaceAllT_clicked();
	void on_searchHere_textChanged(const QString &arg1);

	void on_notes_currentChanged(int index);
	void on_notes_tabCloseRequested(int index);
	void on_searchHere_returnPressed();

    void on_activitiesList_itemDoubleClicked(QListWidgetItem *item);
	void on_sFont_clicked();
	void on_chkRegEx_toggled(bool checked);

protected:
    bool event( QEvent *event ) override;
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::corepad *ui;
    CColorScheme *scheme;
    settings *smi;
    QFont textViewFont;
    bool activities, windowMaximized;
    int docCount, uiMode;
    QSize toolsIconSize, listViewIconSize, windowSize;
    QLabel *lineWordCountLbl;
	QThread textSearchThread;
	bool isSearchThreadRunning = false;
	searchText searchTextObj;
	QMimeDatabase mimedb;
	QFileSystemWatcher watcher;

	void loadSettings();
	void startSetup();
	void shotcuts();
    void setupIcons();
    void loadActivities();

	void openFileDialog();
	bool initializeNewTab(const QString &filePath);
	void setColorScheme();
	void updateShortcut();
	QStringList unsavedFiles();
	void findS(QString searchS, bool reverse, bool isRegEx, QTextDocument::FindFlags flag = QTextDocument::FindFlags());
	void updateTab(QString);
	void newText();
	void hideSearch();

	int navigateTab(bool next);
	void nextTab();
	void previousTab();

	bool closeCurrentTab();
	bool closeTab(int index);
	void closeAllTabs();
	void quitClicked();

	void startCountingMatches();
	void updateLineColCount();

    CEditor *currentEditor();
};
