/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <cmath>

#include <QFile>
#include <QFileDialog>
#include <QShortcut>
#include <QFileInfo>
#include <QDate>
#include <QMessageBox>
#include <QScreen>
#include <QToolButton>
#include <QRegularExpression>
#include <QFile>
#include <QTimer>
#include <QFontDialog>

#include <cprime/messageengine.h>
#include <cprime/filefunc.h>
#include <cprime/themefunc.h>
#include <cprime/shareit.h>
#include <cprime/pinit.h>
#include <cprime/activitesmanage.h>
#include <cprime/sortfunc.h>

#include "corepad.h"
#include "ui_corepad.h"
#include "ceditor.h"
#include "ccolorscheme.h"

// Get which color is bright
bool isDarkTheme(QColor base)
{
	qreal r_gamma = pow( base.redF(), 2.2 );
	qreal g_gamma = pow( base.greenF(), 2.2 );
	qreal b_gamma = pow( base.blueF(), 2.2 );

	qreal luma = r_gamma * 0.2126 + g_gamma * 0.7152 + b_gamma * 0.0722;

	/** Low luminosity => Dark theme */
	if ( luma < 0.5 ) {
		return true;
	}

	else {
		return false;
	}
}

corepad::corepad(QWidget *parent) : QWidget(parent)
  , ui(new Ui::corepad)
  , smi(new settings)
{
	ui->setupUi(this);

	loadSettings();
	startSetup();
    setupIcons();
    shotcuts();
    loadActivities();
}

corepad::~corepad()
{
	if (isSearchThreadRunning) {
		searchTextObj.stopNow(true);
		textSearchThread.quit();
    }

	delete smi;
	delete scheme;
	delete lineWordCountLbl;
	delete ui;
}

/**
 * @brief Loads application settings
 */
void corepad::loadSettings()
{
    // get CSuite's settings
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    activities = smi->getValue("CoreApps", "KeepActivities");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");

    // get app's settings
    textViewFont = smi->getValue("CorePad", "Font");
    windowSize = smi->getValue("CorePad", "WindowSize");
    windowMaximized = smi->getValue("CorePad", "WindowMaximized");
}

/**
 * @brief Setup ui elements
 */
void corepad::startSetup()
{
    // all toolbuttons icon size in sideView
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns) {
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }

	// Used for numbering the document as untitled0.txt
	docCount = -1;

	setColorScheme();

	// Set the transparent color to the main widget
	QPalette p(palette());
	p.setColor(QPalette::Base, Qt::transparent);
	this->setPalette(p);

	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);

    // Add left corner button
	QToolButton *menu = new QToolButton(this);
    menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
    menu->setIconSize(toolsIconSize);
    menu->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
    connect(menu, &QToolButton::clicked, this, &corepad::showSideView);
    ui->notes->setCornerWidget(menu, Qt::TopLeftCorner);
    ui->notes->setCornerWidget(ui->rightMenu, Qt::TopRightCorner);

    // Zoom label
    lineWordCountLbl = new QLabel();
    lineWordCountLbl->setStyleSheet("QLabel{ margin: 5px; padding: 5px; background-color: rgba(0, 0, 0, 0.5); border-radius: 3px; }");
    lineWordCountLbl->setText("Line 20, Words 30.");
    ui->gridLayout->addWidget(lineWordCountLbl, 0, 0, 0, 0, Qt::AlignRight | Qt::AlignBottom);


    if (uiMode == 2) {
        // setup mobile UI

        this->setWindowState(Qt::WindowMaximized);
        ui->activitiesList->setVisible(false);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }

        if(activities){
            loadActivities();
        }
    }

	// Startup page
	ui->page->setCurrentIndex(0);
    ui->sideView->setVisible(false);

    ui->searchBar->setVisible(false);
	ui->cSave->setEnabled(false);
	ui->cSaveAs->setEnabled(false);
	ui->search->setEnabled(false);
	ui->addDate->setEnabled(false);
	ui->cPaste->setEnabled(false);
	ui->pinIt->setEnabled(false);
	ui->shareIt->setEnabled(false);

	connect(ui->cOpen, &QToolButton::clicked, this, &corepad::openFileDialog);
    connect(ui->openFiles, &QPushButton::clicked, this, &corepad::openFileDialog);
    connect(ui->closeSearch, &QToolButton::clicked, this, &corepad::hideSearch);
	connect(&watcher, &QFileSystemWatcher::fileChanged, [=](const QString &filepath) {
		QFileInfo info(filepath);
		if (info.exists()) {
			for (int i = 0; i < ui->notes->count(); i++) {
				CEditor *editor = static_cast<CEditor *>(ui->notes->widget(i));
				if (editor && editor->workFilePath() == filepath) {
					if (info.lastModified() != editor->getModifiedTime()) {
						QString message = "The file '%1' has been changed. ";
						if (editor->hasChange()) {
							message += "Want to discard your changes and reload the file?";
						} else {
							message += "Want to reload the file?";
						}

						int ret = QMessageBox::question(this, "File changed", message.arg(info.fileName()));
						if (ret == QMessageBox::Yes) {
							editor->reloadFile();
						}
					}
				}
			}
		} else {
			QString message = "The file '%1' has been deleted. Want to save it or close?";
			QMessageBox msgBox(this);
			QPushButton *msgSaveAs = msgBox.addButton("Save As", QMessageBox::AcceptRole);
			QPushButton *msgClose = msgBox.addButton("Close", QMessageBox::RejectRole);
			msgBox.setText(message.arg(filepath));
			msgBox.setIcon(QMessageBox::Question);
			msgBox.setWindowTitle("File deleted");
			msgBox.setDefaultButton(msgSaveAs);
			msgBox.exec();

			if (msgSaveAs == msgBox.clickedButton()) {
				for (int i = 0; i < ui->notes->count(); i++) {
					CEditor *editor = static_cast<CEditor *>(ui->notes->widget(i));
					if (editor && editor->workFilePath() == filepath) {
						editor->saveFile(true);
						watcher.removePath(filepath);
						watcher.addPath(editor->workFilePath());
						break;
					}
				}
			} else if (msgClose == msgBox.clickedButton()) {
				for (int i = 0; i < ui->notes->count(); i++) {
					CEditor *editor = static_cast<CEditor *>(ui->notes->widget(i));
					if (editor && editor->workFilePath() == filepath) {
						editor->deleteLater();
						ui->notes->removeTab(i);
						watcher.removePath(filepath);
						break;
					}
				}
			}
		}
	});

	// For seaching
	isSearchThreadRunning = false;
	connect(&searchTextObj, &searchText::updateCountText, ui->searchCount, &QLabel::setText, Qt::QueuedConnection);
	connect(&textSearchThread, &QThread::started, &searchTextObj, &searchText::searchNow);
	connect(&textSearchThread, &QThread::finished, [=]() {
		isSearchThreadRunning = false;
	});

	searchTextObj.moveToThread(&textSearchThread);

	ui->sFont->setText(textViewFont.family() + " " + QString::number(textViewFont.pointSize()) + "pt");
}

void corepad::setupIcons(){
    ui->cOpen->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
    ui->cCut->setIcon(CPrime::ThemeFunc::themeIcon( "edit-cut-symbolic", "edit-cut", "edit-cut" ));
    ui->pinIt->setIcon(CPrime::ThemeFunc::themeIcon( "bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new" ));
    ui->shareIt->setIcon(CPrime::ThemeFunc::themeIcon( "document-send-symbolic", "document-send-symbolic", "document-send" ));
    ui->cCopy->setIcon(CPrime::ThemeFunc::themeIcon( "edit-copy-symbolic", "edit-copy", "edit-copy" ));
    ui->cPaste->setIcon(CPrime::ThemeFunc::themeIcon( "edit-paste-symbolic", "edit-paste", "edit-paste" ));
    ui->search->setIcon(CPrime::ThemeFunc::themeIcon( "edit-find-symbolic", "edit-find", "edit-find" ));
    ui->cNew->setIcon(CPrime::ThemeFunc::themeIcon( "document-new-symbolic", "document-new", "document-new" ));
    ui->cUndo->setIcon(CPrime::ThemeFunc::themeIcon( "edit-undo-symbolic", "edit-undo", "edit-undo" ));
    ui->cRedo->setIcon(CPrime::ThemeFunc::themeIcon( "edit-redo-symbolic", "edit-redo", "edit-redo" ));
    ui->previousW->setIcon(CPrime::ThemeFunc::themeIcon("go-previous-symbolic", "go-previous", "go-previous" ));
    ui->nextW->setIcon(CPrime::ThemeFunc::themeIcon("go-next-symbolic", "go-next", "go-next" ));
    ui->closeSearch->setIcon(CPrime::ThemeFunc::themeIcon( "window-close-symbolic", "window-close", "window-close" ));
    ui->addDate->setIcon(CPrime::ThemeFunc::themeIcon( "document-edit-symbolic", "edit-rename", "document-edit" ));
    ui->sFont->setIcon(CPrime::ThemeFunc::themeIcon( "font-select-symbolic", "font-select", "font-select" ));
    ui->cSave->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-symbolic", "document-save", "document-save" ));
    ui->cSaveAs->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-as-symbolic", "document-save-as", "document-save-as" ));
}

void corepad::shotcuts()
{
	QShortcut *shortcut;
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_N), this);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cNew_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this);
	connect(shortcut, &QShortcut::activated, this, &corepad::openFileDialog);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), ui->app);
    // connect(shortcut, &QShortcut::activated, this, &corepad::on_cSave_clicked);
    // shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cSaveAs_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cUndo_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Y), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cRedo_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_C), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cCopy_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_X), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cCut_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_V), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_cPaste_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), this);
	connect(shortcut, &QShortcut::activated, this, &corepad::quitClicked);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_F), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::on_search_clicked);
	shortcut = new QShortcut(QKeySequence(Qt::Key_Escape), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::hideSearch);
	shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_W), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::closeCurrentTab);
    // shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_W), ui->app);
    // connect(shortcut, &QShortcut::activated, this, &corepad::closeAllTabs);
    // shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_T), ui->app);
    // connect(shortcut, &QShortcut::activated, this, &corepad::on_cNew_clicked);
    // shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::ALT + Qt::Key_PageUp), ui->app);
    // connect(shortcut, &QShortcut::activated, this, &corepad::previousTab);
    // shortcut = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Left), ui->app);
    // connect(shortcut, &QShortcut::activated, this, &corepad::previousTab);
    // shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::ALT + Qt::Key_PageDown), ui->app);
    // connect(shortcut, &QShortcut::activated, this, &corepad::nextTab);
	shortcut = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Right), ui->app);
	connect(shortcut, &QShortcut::activated, this, &corepad::nextTab);
}

void corepad::loadActivities()
{
    if(not activities){
        ui->activitiesList->setVisible(false);
        return;
    }

    ui->activitiesList->clear();

    QSettings recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);
	QStringList topLevel = recentActivity.childGroups();

	if (topLevel.count()) {
		topLevel = CPrime::SortFunc::sortDate(topLevel, Qt::DescendingOrder);
	}

	QListWidgetItem *item;

	Q_FOREACH (QString group, topLevel) {
		recentActivity.beginGroup(group);
		QStringList keys = recentActivity.childKeys();
		keys = CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder, "hh.mm.ss.zzz");

		Q_FOREACH (QString key, keys) {
			QStringList value = recentActivity.value(key).toString().split("\t\t\t");
			if (value[0] == "corepad") {
				QString filePath = value[1];
				if (not CPrime::FileUtils::exists(filePath))
					continue;

				QString baseName = CPrime::FileUtils::baseName(filePath);
				item = new QListWidgetItem(baseName);
				item->setData(Qt::UserRole, filePath);
				item->setToolTip(filePath);
                item->setIcon(CPrime::ThemeFunc::getFileIcon(filePath));
                ui->activitiesList->addItem(item);
			}
		}

		recentActivity.endGroup();
	}
}

void corepad::openFileDialog()
{
	QString openFrom = QDir::homePath() + "/Documents";
	if (currentEditor()) {
		openFrom = currentEditor()->workFilePath();
	}

	QStringList fileP = QFileDialog::getOpenFileNames(this, tr("Open File"), openFrom, tr("All Files (*)"));

	if (!fileP.count()) {
		// Function from utilities.cpp
		CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "Empty File Name", "Select a valid file.");
		return;
	} else {
		int failed = 0;

		for (QString path : fileP) {
			bool open = initializeNewTab(path);

			if (!open) {
				failed++;
			}
		}

		if (failed) {
			CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "Can't open some file(s)", "Invalid file or corrupted.");
		} else {
			CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "File(s) Opened successfully", "Do your changes");
		}
	}
}

bool corepad::initializeNewTab(const QString &filePath)
{
    if (!filePath.length()) {
		CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "Empty file path!!!", "Nothing to worry, select a valid file.");
		return false;
	}

	ui->page->setCurrentIndex(1);

	CEditor *edit = nullptr;

	// Check whether the file is already opened
	for (int i = 0; i < ui->notes->count(); i++) {
		CEditor *tmp = static_cast<CEditor *>(ui->notes->widget(i));

		if (tmp->workFilePath() == filePath) {
			edit = tmp;
			break;
		}
	}

	if (!edit) {
		QFont font = textViewFont;
		edit = new CEditor(filePath, scheme, this);
		edit->setFocusPolicy(Qt::ClickFocus);
		edit->setFont(font);
		edit->lineNumberArea()->setFont(font);

		connect(edit, &CEditor::copyAvailable, this, &corepad::updateShortcut);
		connect(edit->document(), &QTextDocument::undoAvailable, this, &corepad::updateShortcut);
		connect(edit->document(), &QTextDocument::redoAvailable, this, &corepad::updateShortcut);
		connect(edit, &CEditor::fileLoaded, this, &corepad::updateTab);
		connect(edit, &CEditor::unsavedChanges, this, &corepad::updateTab);
		connect(edit, &CEditor::cursorPositionChanged, this, &corepad::updateLineColCount);
		ui->notes->addTab(edit, filePath);

        ui->notes->setTabIcon(ui->notes->count() - 1, CPrime::ThemeFunc::getFileIcon(filePath));
        ui->notes->setIconSize(toolsIconSize);

		edit->loadFile(filePath);
		setWindowTitle(ui->notes->tabText(ui->notes->currentIndex()) + " - CorePad");

        if (QFileInfo::exists(filePath))
			watcher.addPath(filePath);
	}

	edit->setFocus();
	ui->notes->setCurrentWidget(edit);

	return true;
}

void corepad::setColorScheme()
{
	scheme = new CColorScheme;

	QString theme = "";

	if (isDarkTheme(qApp->palette().color(QPalette::Window))) {
		theme = "dark";
	} else {
		theme = "light";
	}

	qDebug() << "Theme Name " << theme;
	scheme = new CColorScheme(QString(":/syntax/%1.colorscheme").arg(theme));
}

void corepad::updateShortcut()
{
	CEditor *edit = currentEditor();

	// Check copy availability
	bool copy = edit->isCopyAvailable();
	ui->cCopy->setEnabled(copy);
	ui->cCut->setEnabled(copy);

	// Check undo availability
	ui->cUndo->setEnabled(edit->document()->isUndoAvailable());

	// Check redo availablity
	ui->cRedo->setEnabled(edit->document()->isRedoAvailable());
}

QStringList corepad::unsavedFiles()
{
	QStringList unsaved;

	for (int i = 0; i < ui->notes->count(); i++) {
		CEditor *tmp = static_cast<CEditor *>(ui->notes->widget(i));

		if (tmp->hasChange()) {
			unsaved << tmp->workFilePath();
		}
	}

	return unsaved;
}

void corepad::findS(QString searchS, bool reverse, bool isRegEx, QTextDocument::FindFlags flag)
{
    if (searchS.length()) {
		CEditor *curr = currentEditor();

		bool found;
		if (isRegEx) {
			found = curr->find(QRegularExpression(searchS), flag);
		} else {
			found = curr->find(searchS, flag);
		}

		if (!found) {
			curr->moveCursor(reverse ? QTextCursor::End : QTextCursor::Start);
			if (isRegEx) {
				found = curr->find(QRegularExpression(searchS), flag);
			} else {
				found = curr->find(searchS, flag);
			}
		}
	}
}

void corepad::updateTab(QString file)
{
	CEditor *curr = nullptr;
	int currIndex = -1;

	for (int i = 0; i < ui->notes->count(); i++) {
		CEditor *tmp = static_cast<CEditor *>(ui->notes->widget(i));

		if (tmp->workFilePath() == file) {
			curr = tmp;
			currIndex = i;
			break;
		}
	}

	if (!curr) {
		return;
	}

    QString tabText = QFileInfo(file).fileName();
    if (uiMode == 2) {
        tabText = QString::number(currIndex); ;
    }

	bool changes = curr->hasChange();
    ui->notes->setTabText(currIndex, (changes ? "*" : "") + tabText);
	setWindowTitle(ui->notes->tabText(currIndex) + " - CorePad");
}

void corepad::newText()
{
	initializeNewTab(QString("new%1.txt").arg(++docCount));
}

void corepad::hideSearch()
{
    lineWordCountLbl->show();
    ui->searchBar->setVisible(false);
	ui->searchHere->clear();
	searchTextObj.stopNow(true);

	CEditor *curr = currentEditor();

	if (!curr) {
		return;
	}

	QTextCursor tcur = curr->textCursor();
	tcur.clearSelection();
	curr->setTextCursor(tcur);
	curr->setFocus();
}

int corepad::navigateTab(bool next)
{
	int index = ui->notes->currentIndex();
	int len = ui->notes->count();

	if (next) {
		index++;

		if (index >= len) {
			index = 0;
		}
	} else {
		index--;

		if (index < 0) {
			index = len - 1;
		}
	}

	return index;
}

void corepad::nextTab()
{
	int index = navigateTab(true);
	ui->notes->setCurrentIndex(index);
	on_notes_currentChanged(index);
}

void corepad::previousTab()
{
	int index = navigateTab(false);
	ui->notes->setCurrentIndex(index);
	on_notes_currentChanged(index);
}

bool corepad::closeCurrentTab()
{
	return closeTab(ui->notes->currentIndex());
}

bool corepad::closeTab(int index)
{
	CEditor *edit = static_cast<CEditor *>(ui->notes->widget(index));

	if (edit == nullptr) {
		return false;
	}

	QString filePath = edit->workFilePath();

	if (edit->hasChange()) {
		int reply = QMessageBox::question(this, tr("Unsaved Changes?"),
										  QString(tr("This file has unsaved changes.\nDo you want to discard it?\n\n%1"))
										  .arg(filePath), QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

		if (reply != QMessageBox::Yes) {
			return false;
		}
	}

    if (QFileInfo::exists(filePath) && activities) {
        CPrime::ActivitiesManage::saveToActivites("corepad", QStringList()<< filePath);
	}

	edit->deleteLater();
	ui->notes->removeTab(index);

	if (index == ui->notes->count()) {
		index -= 1;
	}

	watcher.removePath(filePath);

	on_notes_currentChanged(index);
	return true;
}

void corepad::closeAllTabs()
{
	for (int i = 0; i < ui->notes->count(); i++) {
		on_notes_tabCloseRequested(i);
	}
}

void corepad::quitClicked()
{
	int tabCount = ui->notes->count();
	int cc = 0;

	for (int i = 0; i < tabCount; i++) {
		if (closeTab(i - cc)) {
			cc++;
		}
	}

	if (ui->notes->count() == 0) {
		this->close();
	} else {
		CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "Can't close", "There are still some changes needs to be saved.");
	}
}

void corepad::startCountingMatches()
{
	if (isSearchThreadRunning) {
		searchTextObj.stopNow(true);
		textSearchThread.quit();
		textSearchThread.wait();
	}

	CEditor *curr = currentEditor();
	if (curr == nullptr)
		return;

	QString searchKeyword = ui->searchHere->text();

    if (!searchKeyword.length()) {
		QTimer::singleShot(300, [=]() {
			ui->searchCount->setText("0 matches");
		});
		return;
	}

	isSearchThreadRunning = true;
	searchTextObj.setSearchCriteria(searchKeyword, curr->toPlainText(), ui->chkRegEx->isChecked());
	textSearchThread.start();
}

void corepad::updateLineColCount()
{
	auto editor = currentEditor();
	if (!editor)
		return;

	lineWordCountLbl->setText(QString("Line %1, Col %2")
							  .arg(editor->textCursor().blockNumber() + 1)
							  .arg(editor->textCursor().columnNumber() + 1));
}


bool corepad::event(QEvent *event)
{
	if ( event->type() == QEvent::ApplicationPaletteChange ) {
		// Set the color scheme to editor
		setColorScheme();
		for( int i = 0; i < ui->notes->count(); i++ ) {
			CEditor *editor = qobject_cast<CEditor *>( ui->notes->currentWidget() );
			if ( editor ) {
				editor->updateColorScheme( scheme );
			}
		}
	}

	return QWidget::event(event);
}

void corepad::showSideView()
{
    if (ui->sideView->isVisible()) {
        ui->sideView->setVisible(0);
    } else {
        ui->sideView->setVisible(1);
    }
}

void corepad::on_newTextW_clicked()
{
	newText();
}

void corepad::on_cNew_clicked()
{
	newText();
}

bool corepad::on_cSave_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return false;
	}

	bool newFile = QFileInfo(curr->workFilePath()).exists();
	bool saved = curr->saveFile();

	if (saved) {
		if (!newFile)
			watcher.addPath(curr->workFilePath());
		// Update the tab icon while saved the file in a different file format
		ui->notes->setTabIcon(ui->notes->currentIndex(), CPrime::ThemeFunc::getFileIcon(curr->workFilePath()));
	}

	return saved;
}

void corepad::on_cSaveAs_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

	QString oldFilePath = curr->workFilePath();
	bool newFile = QFileInfo(curr->workFilePath()).exists();
	bool saved = curr->saveFile(true);

	if (saved) {
		if (!newFile) {
			watcher.removePath(oldFilePath);
			watcher.addPath(curr->workFilePath());
		}

		// Update the tab icon while saved the file in a different file format
		ui->notes->setTabIcon(ui->notes->currentIndex(), CPrime::ThemeFunc::getFileIcon(curr->workFilePath()));
	}
}

void corepad::on_cCopy_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

	curr->copy();
}

void corepad::on_cPaste_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

	curr->paste();
}

void corepad::on_cCut_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

	curr->cut();
}

void corepad::on_cUndo_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

	curr->undo();
}

void corepad::on_cRedo_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

	curr->redo();
}

void corepad::on_addDate_clicked()
{
	currentEditor()->insertPlainText(QDate::currentDate().toString("dd/MM/yyyy"));
}

void corepad::on_search_clicked()
{
	CEditor *curr = currentEditor();

	if (curr == nullptr) {
		return;
	}

    if (ui->searchBar->isVisible() && !curr->textCursor().selectedText().length()) {
		hideSearch();
        lineWordCountLbl->show();
	} else {
        ui->searchBar->setVisible(true);
		ui->searchHere->setFocus();
		QString search = curr->textCursor().selectedText();

        if (search.length()) {
			ui->searchHere->setText(search);
		}
		lineWordCountLbl->hide();
	}
}

void corepad::on_pinIt_clicked()
{
	QString workFilePath = currentEditor()->workFilePath();

    if (workFilePath.length()) {
        if (!QFileInfo::exists(workFilePath)) {
			CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "File not exists or saved", "Please choose a valid file or save the file first to pin it.");
			return;
		}

        PinIT *pit = new PinIT(QStringList() << workFilePath, this);
        pit->exec();
	}
}

void corepad::on_shareIt_clicked()
{
	QString workFilePath = currentEditor()->workFilePath();

    if (workFilePath.length()) {
        if (!QFileInfo::exists(workFilePath)) {
			// Function from utilities.cpp
			CPrime::MessageEngine::showMessage("cc.cubocore.CorePad", "CorePad", "File not exists or saved", "Please choose a valid file or save the file first to share it.");
			return;
		}

        ShareIT *t = new ShareIT(QStringList() << workFilePath, listViewIconSize, nullptr);
        // Set ShareIT window size
        if (uiMode == 2 )
            t->setFixedSize(QGuiApplication::primaryScreen()->size() * .8 );
        else
            t->resize(500,600);

        t->exec();
	}
}

void corepad::on_nextW_clicked()
{
	findS(ui->searchHere->text(), false, ui->chkRegEx->isChecked());
}

void corepad::on_previousW_clicked()
{
	findS(ui->searchHere->text(), true, ui->chkRegEx->isChecked(), QTextDocument::FindBackward);
}

void corepad::on_replaceT_clicked()
{
	CEditor *curr = currentEditor();

	if (curr->textCursor().selectedText() == ui->searchHere->text()) {
		curr->insertPlainText(ui->replaceWith->text());
	}

	startCountingMatches();
	on_nextW_clicked();
}

void corepad::on_replaceAllT_clicked()
{
	CEditor *curr = currentEditor();
	QString selected = curr->textCursor().selectedText();

    if (selected.length()) {
		if (selected == ui->searchHere->text()) {
			curr->insertPlainText(ui->replaceWith->text());
		}
	}

	bool found = curr->find(ui->searchHere->text());

	while (found) {
		curr->insertPlainText(ui->replaceWith->text());
		found = curr->find(ui->searchHere->text());
	}
	startCountingMatches();
}

void corepad::on_searchHere_textChanged(const QString &arg1)
{
	CEditor *curr = currentEditor();

	if (curr == nullptr)
		return;

	startCountingMatches();

    if (!arg1.length()) {
		curr->moveCursor(QTextCursor::StartOfWord);
		return;
	}

	if (curr->textCursor().atEnd()) {
		curr->moveCursor(QTextCursor::Start);
	} else {
		curr->moveCursor(QTextCursor::Start, QTextCursor::KeepAnchor);
	}

	findS(arg1, false, ui->chkRegEx->isChecked());
}

void corepad::on_notes_currentChanged(int index)
{
	bool status;

	if (ui->notes->count() > 0) {
		status = true;

		updateShortcut();

        if (ui->searchBar->isVisible()) {
			on_searchHere_textChanged(ui->searchHere->text());
		}

		setWindowTitle(ui->notes->tabText(index) + " - CorePad");
	} else {
		status = false;

        ui->searchBar->setVisible(false);
		ui->cCopy->setEnabled(false);
		ui->cCut->setEnabled(false);
		ui->cUndo->setEnabled(false);
		ui->cRedo->setEnabled(false);

		setWindowTitle("CorePad");

		// Startup page
		ui->page->setCurrentIndex(0);

        if(uiMode != 2 && activities){
            loadActivities();
        } else{
            ui->activitiesList->setVisible(false);
        }
	}

	ui->cSave->setEnabled(status);
	ui->cSaveAs->setEnabled(status);
	ui->search->setEnabled(status);
	ui->addDate->setEnabled(status);
	ui->cPaste->setEnabled(status);
	ui->pinIt->setEnabled(status);
	ui->shareIt->setEnabled(status);
}

void corepad::on_notes_tabCloseRequested(int index)
{
	ui->notes->setCurrentIndex(index);
	closeTab(index);
}

CEditor *corepad::currentEditor()
{
	if (ui->notes->count() < 1) {
		return nullptr;
	}

	return static_cast<CEditor *>(ui->notes->currentWidget());
}

void corepad::sendFiles(const QStringList &paths)
{
	if (paths.count()) {
		for (QString str : paths) {
			if (CPrime::FileUtils::isFile(str)) {
				initializeNewTab(str);
			} else {
				qDebug() << "func(sendFiles) : Error : Invalid filepath got :" << str;
			}
		}
	}
}

void corepad::on_searchHere_returnPressed()
{
	on_nextW_clicked();
}

void corepad::on_activitiesList_itemDoubleClicked(QListWidgetItem *item)
{
	QString filepath = item->data(Qt::UserRole).toString();
	initializeNewTab(filepath);
}

void corepad::on_sFont_clicked()
{
    QFontDialog *dialog = new QFontDialog(textViewFont, this);
    if (dialog->exec()) {
        textViewFont = dialog->selectedFont();
        ui->sFont->setText(textViewFont.family() + " " + QString::number(textViewFont.pointSize()) + "pt");
        smi->setValue("CorePad", "Font", textViewFont);
        currentEditor()->setFont(textViewFont);
    }
}


void corepad::on_chkRegEx_toggled(bool checked)
{
	Q_UNUSED(checked)
	if (currentEditor()->textCursor().hasSelection())
		currentEditor()->moveCursor(QTextCursor::StartOfWord);
	startCountingMatches();
	on_nextW_clicked();
}

void corepad::closeEvent(QCloseEvent *event)
{
    qDebug() << "Closing...";

    smi->setValue("CorePad", "WindowSize", this->size());
    smi->setValue("CorePad", "WindowMaximized", this->isMaximized());

    QStringList unsaved = unsavedFiles();

    if (unsaved.isEmpty()) {
        while (ui->notes->count()) {
            closeTab(ui->notes->count() - 1);
        }

        QWidget::closeEvent(event);
        return;
    }

    // Otherwise, ask the user what to do.
    QMessageBox::StandardButton but = QMessageBox::question(
        this,
        tr("Save Changes before closing?"),
        QString(tr("There are unsaved changes.\nDo you want save them?\n\n%1")).arg(unsaved.join("\n")),
        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
        QMessageBox::Yes);

    if (but == QMessageBox::Cancel) {
        event->ignore();
        return;
    } else if (but == QMessageBox::Yes) {
        // Save all opened files
        bool ok = true;

        for (int i = 0; i < ui->notes->count() && ok; i++) {
            CEditor *tmp = static_cast<CEditor *>(ui->notes->widget(i));

            if (tmp->hasChange()) {
                ok = ok && tmp->saveFile();
            }
        }

        if (!ok) {
            //cancelled by user
            event->ignore();
            return;
        }
    }

    QWidget::closeEvent(event);
}

void searchText::setSearchCriteria(const QString &keyword, const QString &contents, bool isRegEx)
{
	this->keyword = keyword;
	this->contents = contents;
	this->isRegEx = isRegEx;
}

void searchText::searchNow()
{
    if (!keyword.length() && !contents.length()) {
		emit updateCountText("0 matches");
		return;
	}

	breakNow = false;

	int count = 0;
	QString pattern = keyword;
	if (!isRegEx) {
		pattern = QRegularExpression::escape(keyword);
	}
	QRegularExpression expr(pattern, QRegularExpression::CaseInsensitiveOption);

	if (expr.isValid()) {
		QRegularExpressionMatch matcher;
		int index = contents.indexOf(expr, 0, &matcher);
		while (!breakNow && index >= 0 && index <= contents.length()) {
			count++;
			emit updateCountText(QString("%1 match%2").arg(count).arg(count > 1 ? "es" : ""));

			index += matcher.capturedLength();
			index = contents.indexOf(expr, index, &matcher);
		}
	}

	emit updateCountText(QString("%1 match%2").arg(count).arg(count > 1 ? "es" : ""));
	keyword.clear();
	contents.clear();
}

void searchText::stopNow(bool closeNow)
{
	this->breakNow = closeNow;
}
