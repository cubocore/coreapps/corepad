/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QVector>
#include <QRegularExpression>

class CColorScheme;

struct CSyntaxRule {
    QRegularExpression begin;
    QRegularExpression end;
    QRegularExpression pattern;
    QTextCharFormat format;
};

class CSyntaxHighlighter : public QSyntaxHighlighter {
    Q_OBJECT

public:
    CSyntaxHighlighter(QTextDocument *doc, QString fileName, CColorScheme *colorScheme);

    void updateColorScheme(CColorScheme *colorScheme, QString syntaxFile);

private:
    QVector<CSyntaxRule> syntax;
    QVector<CSyntaxRule> *loadSyntaxFile(QString fileName);
    CColorScheme *scheme;

protected:
    void highlightBlock(const QString &text) override;

};
