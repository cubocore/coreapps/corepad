/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QFileInfo>
#include <QFileDialog>
#include <QDebug>
#include <QPainter>
#include <QTextBlock>
#include <QSettings>
#include <QScroller>
#include <QShortcut>
#include <QScrollBar>
#include <QScrollArea>

#include <cprime/themefunc.h>

#include "ceditor.h"
#include "ccolorscheme.h"
#include "csyntaxhighlighter.h"


CEditor::CEditor(QString fPath, CColorScheme *colorScheme, QWidget *parent) : QPlainTextEdit(parent), smi(new settings)

{
	m_filePath = fPath;
	scheme = colorScheme;
	currentLineColor = QColor(scheme->getCurrentLineColor());

	m_lineNumberArea = new LineNumberArea(this);
	m_lineNumberArea->setContentsMargins(0, 0, 0, 0);

	// Connecting to slots and signals
	connect(this, &CEditor::blockCountChanged, this, &CEditor::updateLineNumberAreaWidth);
	connect(this, &CEditor::updateRequest, this, &CEditor::updateLineNumberArea);
	connect(this, &CEditor::cursorPositionChanged, this, &CEditor::highlightCurrentLine);
	connect(this, &CEditor::textChanged, this, &CEditor::textChangedUpdate);

	connect(this, &CEditor::copyAvailable, this, &CEditor::setCopyAvailable);
	setFrameShape(QFrame::NoFrame);

	updateLineNumberAreaWidth();   //Set the line number area.
	highlightCurrentLine();
	loadSyntax();

	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key_Insert), this);
	connect(shortcut, &QShortcut::activated, this, &CEditor::toggleOverwrite);

	setTabStopDistance(QFontMetricsF(font()).horizontalAdvance(' ') * 4);

    int uiMode = smi->getValue("CoreApps", "UIMode");
    if (uiMode != 0) {
        QScroller::grabGesture(this, QScroller::LeftMouseButtonGesture);
    }

}

CEditor::~CEditor()
{
	delete chighlighter;
}

void CEditor::updateColorScheme(CColorScheme *colorScheme) {
	if (chighlighter) {
		QString file = QFileInfo(m_filePath).suffix();

        if (file.length() <= 0) {
			return;
		}

		QString syntaxFile = getSyntaxFile(file);

        if (!syntaxFile.length()) {
			return;
		}

		chighlighter->updateColorScheme(colorScheme, syntaxFile);
	}
}

void CEditor::loadFile(const QString &filePath)
{
	m_filePath = filePath;
	this->clear();

	QFile file(filePath);
	QString str;

	if (file.open(QIODevice::Text | QIODevice::ReadOnly)) {
		QTextStream in(&file);

		while (!in.atEnd()) {
			str += QString(in.readLine() + "\n");
		}

		file.close();
	}

	setPlainText(str);
	str.clear();
	m_modifiedTime = QFileInfo(filePath).lastModified();

	emit fileLoaded(m_filePath);
}

bool CEditor::saveFile(bool newFile)
{
	if (!newFile && m_filePath.startsWith("/")) {
		if (!QFileInfo(m_filePath).isWritable() && QFileInfo(m_filePath).exists()) {
			newFile = true;
		}
	}

	if (!m_filePath.startsWith("/") || newFile) {
		QString fileP = QFileDialog::getSaveFileName(this, tr("Save File"), m_filePath, tr("Text File (*)"));

		if (fileP.isEmpty()) {
			return false;    //cancelled
		}

		m_filePath = fileP;
	}

	QStringList contents = toPlainText().split("\n");
	bool overwrite = true;
	QFile file(m_filePath);

	if (file.exists() && !overwrite) {
		return false;
	}

	bool ok = false;

	if (contents.isEmpty()) {
		contents << "\n";
	}

	if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
		QTextStream out(&file);
		out << contents.join("\n");

		if (!contents.last().isEmpty()) {
			out << "\n";    //always end with a new line
		}

		file.close();
		m_modifiedTime = QFileInfo(m_filePath).lastModified();
		ok = true;
		this->document()->setModified(false);
	}

	if (ok) {
		emit fileLoaded(m_filePath);
	}

	// Check the suffix whether its changed or not (For optimization)
	loadSyntax();

	return true;
}

void CEditor::reloadFile()
{
	qDebug() << "Reloading file...";
	int position = this->textCursor().position();
	int vScrollPosition = this->verticalScrollBar()->value();
	int hScrollPosition = this->horizontalScrollBar()->value();

	loadFile(m_filePath);

	QTextCursor cursor = this->textCursor();
	cursor.setPosition(position);
	this->setTextCursor(cursor);
	this->verticalScrollBar()->setValue(vScrollPosition);
	this->horizontalScrollBar()->setValue(hScrollPosition);
}

bool CEditor::isCopyAvailable()
{
	return m_copyAvailable;
}

QString CEditor::workFilePath() const
{
	return m_filePath;
}

bool CEditor::hasChange()
{
	return document()->isModified();
}

bool CEditor::canClose()
{
	if (hasChange() && QFileInfo(m_filePath).exists()) {
		return false;
	}

	return true;
}

QDateTime CEditor::getModifiedTime()
{
	return m_modifiedTime;
}

void CEditor::textChangedUpdate()
{
	if (hasChange()) {
		emit unsavedChanges(m_filePath);
	} else {
		emit fileLoaded(m_filePath);
	}
}

void CEditor::highlightCurrentLine()
{
	QList<QTextEdit::ExtraSelection> extraSelections;

	if (!isReadOnly()) {
		QTextEdit::ExtraSelection selection;
		selection.format.setBackground(currentLineColor);
		selection.format.setProperty(QTextFormat::FullWidthSelection, true);
		selection.cursor = textCursor();
		selection.cursor.clearSelection();
		extraSelections.append(selection);
	}

	setExtraSelections(extraSelections);
}

void CEditor::setCopyAvailable(bool yes)
{
	m_copyAvailable = yes;
}

QString CEditor::getSyntaxFile(QString suffix)
{
	QStringList list;
	list << "c" << "h" << "cpp" << "hpp" << "htm" << "cs"
		 << "desktop" << "go" << "html" << "java" << "js"
		 << "md" << "php" << "py" << "sh" << "xml";

	QString syntaxFile = "";

	if (suffix == "h") {
		suffix = "c";
	} else if (suffix == "hpp") {
		suffix = "cpp";
	} else if (suffix == "htm") {
		suffix = "html";
	}

	if (!list.contains(suffix)) {
		return "";
	}

	syntaxFile = QString(":/syntax/%1.xml").arg(suffix);

	return syntaxFile;
}

void CEditor::loadSyntax()
{
	// No need to initialize syntax highlighter while the file is not saved.
	if (!QFileInfo(m_filePath).exists()) {
		return;
	}

	QString file = QFileInfo(m_filePath).suffix();

    if (file.length() <= 0) {
		return;
	}

	QString syntaxFile = getSyntaxFile(file);

    if (!syntaxFile.length()) {
		return;
	}

	chighlighter = new CSyntaxHighlighter(document(), syntaxFile, scheme);
	chighlighter->rehighlight();
}

void CEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
	QPainter painter(m_lineNumberArea);

	// Determine which line number to show
	QTextBlock block = firstVisibleBlock();
	int top = static_cast<int>(blockBoundingGeometry(block).translated(contentOffset()).top());
	int bottom;

	while (block.isValid() && top <= event->rect().bottom()) {
		bottom = static_cast<int>(top + blockBoundingRect(block).height());

		if (block.isVisible() && bottom >= event->rect().top()) {
			painter.drawText(0, top, m_lineNumberArea->width(), fontMetrics().height(), Qt::AlignCenter, QString::number(block.blockNumber() + 1));
		}

		block = block.next();
		top = bottom;
	}

}

void CEditor::wheelEvent(QWheelEvent *event)
{
	QPoint wheelScroll = event->angleDelta();
	if (event->modifiers() & Qt::ControlModifier) {
		if (wheelScroll.x() > 0 || wheelScroll.y() > 0) {
			this->zoomIn();
			lineNumberArea()->setFont(this->font());
		} else {
			this->zoomOut();
			lineNumberArea()->setFont(this->font());
		}
		return;
	}
	QPlainTextEdit::wheelEvent(event);
}

int CEditor::lineNumberAreaWidth()
{
	// Get the number of chars we need for line numbers
	int lines = this->blockCount();

	if (lines < 1) {
		lines = 1;
	}

	int chars = 1;

	while (lines >= 10) {
		chars++;
		lines /= 10;
	}

	return (fontMetrics().boundingRect("9").width() * chars) + 4; //make sure to add a tiny bit of padding
}

void CEditor::updateLineNumberAreaWidth()
{
	setViewportMargins(lineNumberAreaWidth() + 3, 0, 0, 0);
}

void CEditor::updateLineNumberArea(const QRect &rect, int dy)
{
	if (dy != 0) {
		m_lineNumberArea->scroll(0, dy);
	} else {
		m_lineNumberArea->update(0, rect.y(), m_lineNumberArea->width(), rect.height());
	}

	if (rect.contains(viewport()->rect())) {
		updateLineNumberAreaWidth();
	}
}

void CEditor::toggleOverwrite()
{
	this->setOverwriteMode(!overwriteMode());
}

QWidget *CEditor::lineNumberArea() const
{
	return m_lineNumberArea;
}

void CEditor::resizeEvent(QResizeEvent *e)
{
	QPlainTextEdit::resizeEvent(e);
	QRect cr = contentsRect();

	// Geometry of the line number area
	m_lineNumberArea->setGeometry(QRect(cr.left() - 2, cr.top(), lineNumberAreaWidth() + 3, cr.height() + 2));
}
